import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles({
  root: {
    paddingLeft: '80px',
    paddingRight: '80px',
    paddingTop: '20px',
    paddingBottom: '20px',
    fontSize: 30,
    backgroundColor: props => props.color,
  }
});

function Instruction(props) {
  const classes = useStyles(props);
  return (
    <Paper style={{marginTop:'10px', padding:'10px'}}>
      {props.title}
    </Paper>
  );
}

export default function Instructionlist(props) {
  const list = props.procedure.step
  console.log(list);
  return (
    list.map((i) => <Instruction title={i.title} />)
  );
}