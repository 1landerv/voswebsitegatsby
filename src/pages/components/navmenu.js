import React, { Fragment } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Navbutton from './navbutton';

const useStyles = makeStyles({
  root: {
    margin: '10px',
    width: '50%',
    marginLeft: '25%',
    marginRight: '25%'
  }
});

export default function Navmenu() {
  const classes = useStyles();
  return (
    <Fragment>
      <Navbutton classes={{root: classes.root}} title='Aanmelden' color='#a1a18c' route='/login'/><br/>
      <Navbutton classes={{root: classes.root}} title='Brand' color='#a83240' route='/brand'/><br/>
      <Navbutton classes={{root: classes.root}} title='Gif' color='#22782c' route='/gif'/><br/>
      <Navbutton classes={{root: classes.root}} title='Bommelding' color='#a1a137' route='/bommelding'/><br/>
    </Fragment>
  );
}