import React, {Fragment, useState, useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Navbar from './components/navbar';
import Navmenu from './components/navmenu';
import Login from './components/login';
import Instructionlist from './components/instructionlist';
import { Paper } from '@material-ui/core';
import procedureListLDO from './data/procedureListLDO.json';
import { Router } from '@reach/router';

const useStyles = makeStyles(() => ({
  paper: {
    padding: 10,
    marginTop: 20,
    marginLeft: 150,
    marginRight: 150,
    backgroundColor: "#dedcdc"
  },
  paper2: {
    paddingTop: 15,
    paddingBottom: 15,
    marginTop: 20,
    marginLeft: 150,
    marginRight: 150,
    backgroundColor: "#ffffff"
  }
}));

export default function Home() {
  const classes = useStyles();
  const [location, setLocation] = useState({latitude: 0, longitude: 0});
  
  useEffect(() => {
    navigator.geolocation.getCurrentPosition((position) => {
    setLocation({latitude: position.coords.latitude, longitude: position.coords.longitude})
    });
    console.log(location);
  },[location.latitude,location.longitude]);

  return <Fragment>
    <Navbar/>
    <Paper className={classes.paper} square>
      <Router>
        <Navmenu path="/navmenu"/>
        <Login path="/login"/>
        <Instructionlist path="/brand" procedure={procedureListLDO.procedure[0]}/>
      </Router>
    </Paper>
  </Fragment>
}
